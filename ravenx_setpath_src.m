function ravenx_setpath_src(mode)
%  Routine:
%       SetPath_src
%
%  Inputs:
%       mode
%  Outputs:
%       none, but paths for ASE_Sedna are set up
%  Description:
%       SetPath_src, routine establishes paths for the PRBA libraries.
%
% History
%   PDugan       January 2009       Ver 1.0    January 2009
%   JZollweg     March 2011         Ver 1.1    Make usable on non-PCs
%   PDugan       March 2011         Ver 1.2    Added mode variable for
%                                              developers
%   JZollweg     December 2011      Ver 1.3    Made mode variable an input
%   CPopescu     Feb4 2013                     Reverted back
%   JZollweg     August 2014        Ver 2.0    Add batch mode
%   JZollweg     July 2018          Ver 2.1    Customized for RavenX-NA
%   PDugan       Sept 2018                     Merged NA and AD for SPAWAR
%                                              integration.
%   PDugan       3-5-18                        Simplified path scripts
%   PDugan       10-5-19                       Phone config


if nargin < 1
    mode = 'desktop';
end

if ~any(strcmp(mode, {'desktop'}))
    mode = 'desktop';
end

% create the RavenX project folders
home_dir = fileparts(mfilename('fullpath')); path([home_dir], path); % root path
